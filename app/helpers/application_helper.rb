module ApplicationHelper
  def full_title(page_title: "")
    if page_title.blank?
      Settings.TITLE
    else
      "#{page_title} - #{Settings.title}"
    end
  end
end
