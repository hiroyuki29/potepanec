module Mystore::ProductDecorator
  def related_products
    Spree::Product.in_taxons(taxons).where.not(id: id).group(:id).
      order('min(spree_taxons.id)')
  end

  Spree::Product.prepend self
end
