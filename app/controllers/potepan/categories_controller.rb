class Potepan::CategoriesController < ApplicationController
  def show
    @taxonomies = Spree::Taxonomy.preload(taxons: :products)
    @taxon = Spree::Taxon.find(params[:id])
    @products = @taxon.all_products.preload(master: [:default_price, :images])
  end
end
