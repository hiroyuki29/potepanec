class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.related_products.limit(Settings.max_related_products_number).
      preload(master: [:default_price, :images])
  end

  def index
    @taxonomies = Spree::Taxonomy.preload(taxons: :products)
    @products = Spree::Product.preload(master: [:default_price, :images])
  end
end
