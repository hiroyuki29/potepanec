require 'rails_helper'

RSpec.feature 'category page' do
  let(:taxon) { create(:taxon, name: 'taxon_test1') }
  let(:taxon2) { create(:taxon, name: 'taxon_test2') }
  let(:taxon3) { create(:taxon, name: 'taxon_test3') }
  let!(:link_check_product) { create(:product, name: 'link_check', taxons: [taxon]) }

  before do
    create(:product, name: 'test1', taxons: [taxon])
    create(:product, name: 'test2', taxons: [taxon2])
    visit potepan_category_path(taxon.id)
  end

  scenario 'include taxon_name in title' do
    expect(page).to have_title "#{taxon.name} - BIGBAG Store"
  end

  scenario 'has name of products which have the same taxon id' do
    within('.product-list') do
      expect(page).to have_content 'test1'
    end
  end

  scenario 'does not has name of products which do not have the same taxon id' do
    within('.product-list') do
      expect(page).not_to have_content 'test2'
    end
  end

  scenario 'show number of products in category box' do
    within('#category-window') do
      expect(page).to have_content 'taxon_test1(2)'
      expect(page).to have_content 'taxon_test2(1)'
    end
  end

  scenario 'has correct link to product show page' do
    click_on 'link_check'
    expect(current_path).to eq potepan_product_path(link_check_product.id)
  end
end
