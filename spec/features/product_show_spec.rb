require 'rails_helper'

RSpec.feature 'product page' do
  let(:taxon) { create(:taxon, name: "test1") }
  let(:taxon2) { create(:taxon, name: "test2") }
  let(:taxon3) { create(:taxon, name: "test3") }
  let(:product) { create(:product, name: "test", taxons: [taxon, taxon2]) }
  let(:product_not_taxon) { create(:product, taxons: []) }

  before do
    visit potepan_product_path(product.id)
  end

  scenario 'include product_name in title' do
    expect(page).to have_title "#{product.name} - BIGBAG Store"
  end

  scenario 'show name of product which has the same product id' do
    within('#product-name-show') do
      expect(page).to have_content product.name
    end
  end

  scenario 'show price of product which has the same product id' do
    within('#product-price-show') do
      expect(page).to have_content product.price
    end
  end

  scenario 'show description of product which has the same product id' do
    within('#product-description-show') do
      expect(page).to have_content product.description
    end
  end

  scenario 'has link to category page if product has taxons' do
    click_on '一覧ページへ戻る'
    expect(current_path).to eq potepan_category_path(product.taxons.first.id)
  end

  scenario 'has link to all products page if product has not taxons' do
    visit potepan_product_path(product_not_taxon.id)
    click_on '一覧ページへ戻る'
    expect(current_path).to eq potepan_products_path
    expect(page).to have_title 'All_products - BIGBAG Store'
  end

  feature 'for related-items check' do
    let(:main_taxon) { create(:taxon, name: "main") }
    let(:related_taxon) { create(:taxon, name: "related") }
    let(:unrelated_taxon) { create(:taxon, name: "unrelated") }
    let(:only_3_related_taxon) { create(:taxon, name: "only_3_related") }
    let(:link_check_taxon) { create(:taxon, name: "link_check") }
    let(:main_product) { create(:product, name: "main", taxons: [main_taxon, related_taxon]) }
    let(:has_only_3_related_products) { create(:product, taxons: [only_3_related_taxon]) }

    before do
      create_list(:product, 5, taxons: [related_taxon], name: "related-item-name")
      create_list(:product, 1, taxons: [unrelated_taxon])
      create_list(:product, 3, taxons: [only_3_related_taxon])
      visit potepan_product_path(main_product.id)
    end

    scenario 'does not show main product name in related item box' do
      within(".related-items-box") do
        expect(page).not_to have_content main_product.name
      end
    end

    scenario 'show related product name in related item box' do
      within(".related-items-box") do
        expect(page).to have_content "related-item-name"
      end
    end

    context 'when there are over 4 items that related to viewing item, ' do
      scenario 'shows 4 related items' do
        expect(page.all(".related-item").count).to eq 4
      end
    end

    context 'when there are only 3 items that related to viewing item, ' do
      scenario 'shows only 3 related items' do
        visit potepan_product_path(has_only_3_related_products.id)
        expect(page.all(".related-item").count).to eq 3
      end
    end

    context 'when product has no taxon, ' do
      scenario 'shows no related items' do
        visit potepan_product_path(product_not_taxon.id)
        expect(page.all(".related-item").count).to eq 0
      end
    end

    context 'when product has related product' do
      let(:has_link_check_product) { create(:product, taxons: [link_check_taxon]) }
      let!(:link_check_product) { create(:product, name: "link_check", taxons: [link_check_taxon]) }

      scenario 'has link to related product' do
        visit potepan_product_path(has_link_check_product.id)
        find(".link_check-related").click
        expect(current_path).to eq potepan_product_path(link_check_product.id)
      end
    end
  end
end
