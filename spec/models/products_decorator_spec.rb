require 'rails_helper'

RSpec.describe 'ProductDecorator', type: :model do
  describe 'related_products method' do
    let(:main_taxon) { create(:taxon, name: 'main') }
    let(:related_taxon) { create(:taxon, name: 'related') }
    let(:unrelated_taxon) { create(:taxon, name: 'unrelated') }
    let(:main_product) { create(:product, name: 'main', taxons: [main_taxon, related_taxon]) }
    let(:related_product) { create(:product, taxons: [related_taxon]) }
    let(:unrelated_product) { create(:product, taxons: [unrelated_taxon]) }

    it 'returns records without receiver' do
      expect(main_product.related_products).not_to include main_product
    end

    it 'returns records which taxon is same as recievers one' do
      expect(main_product.related_products).to include related_product
    end

    it 'does not return records which taxon is not same as recievers one' do
      expect(main_product.related_products).not_to include unrelated_product
    end

    context 'changes order' do
      let!(:first_taxon) { create(:taxon, name: 'first') }
      let!(:second_taxon) { create(:taxon, name: 'second') }
      let(:main2_product) { create(:product, taxons: [first_taxon, second_taxon]) }
      let!(:young_id_product) { create(:product, taxons: [second_taxon]) }
      let!(:old_id_product) { create(:product, taxons: [first_taxon]) }

      it 'by taxon id' do
        expect(main2_product.related_products.first).to eq old_id_product
      end
    end
  end
end
