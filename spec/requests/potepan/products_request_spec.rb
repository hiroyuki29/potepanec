require 'rails_helper'

RSpec.describe 'Potepan::Products', type: :request do
  describe 'GET #show' do
    let(:main_taxon) { create(:taxon, name: 'main') }
    let(:related_taxon) { create(:taxon, name: 'related') }
    let(:main_product) { create(:product, name: 'main', taxons: [main_taxon, related_taxon]) }
    let!(:related_product) { create(:product, name: 'related', taxons: [related_taxon]) }

    it 'returns http success' do
      get potepan_product_path(main_product.id)
      expect(response).to have_http_status(:success)
    end

    it 'show related_products name' do
      get potepan_product_path(main_product.id)
      expect(response.body).to include 'related'
    end
  end

  describe 'GET #index' do
    it 'returns http success' do
      get potepan_products_path
      expect(response).to have_http_status(:success)
    end
  end
end
